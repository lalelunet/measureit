#!/bin/sh

# This is a dirty script to help me build a measureit image
# into a fresh raspian without much effort. It does not help you when you have problems with measureit!
# It is quick and dirty. Use it on your own risk. 
# If you run this script on a always configured system it will damage it. I promise ;)

# download and install latest raspian lite version
# extract it and dd it to the sd card
#   

# enable ssh server at start
# touch /Volumes/bootfs/ssh
# create tmp te user for use to install # pwd is 123456
# echo 'te:rajbdRK1cU1pQ' > /Volumes/bootfs/userconf.txt

# boot new image in the raspberrypi

## Usage: Log in the new raspian image. 
## Became root with 'sudo su -;bash'

# to make sure that the user get a working image with a pi account and default passwd
# useradd pi
# passwd pi
# raspberry
# /usr/sbin/usermod -G sudo pi

## Install git with 'apt-get install -y git'
## Clone measureit from gitlab
# git clone https://gitlab.com/lalelunet/measureit.git
# cd measureit/measureit_system_files/install
# run this script './install-measureit-image.sh'


printf "\n >> update system >>\n#########################\n"
apt-get update && apt-get -y upgrade
printf "\n >> done >>\n\n"

printf "\n>> install software >>\n######################\n"
apt-get install -y vim daemontools-run php8.2-fpm php8.2-mysql nginx samba samba-common-bin python3-mysqldb python3-serial python3-pip
/usr/bin/pip install paho-mqtt
printf "\n >> done >>\n\n"

printf "\n >> enable ipv6 >>\n#########################\n"
echo "ipv6" >> /etc/modules
printf "\n >> done >>\n\n"

#printf "\n >> Set static ip adresses >>\n#########################\n"
#cat config-static-network >> /etc/network/interfaces
#systemctl daemon-reload
#systemctl restart networking
#printf "\n >> done >>\n\n"

printf "\n >> create supervise directories >>\n#########################\n"
mkdir /etc/servers
ln -s /etc/service /service
printf "\n >> done >>\n\n"

printf "\n >> create directories for php change the default config and start php-fpm service  >>\n#########################\n"
mkdir /web
useradd -d /web -s /bin/bash web
chown -R web:web /web
sed -i -e 's/;daemonize = yes/daemonize = no/g' /etc/php/8.2/fpm/php-fpm.conf
sed -i -e 's/www-data/web/g' /etc/php/8.2/fpm/pool.d/www.conf
printf "\n >> done >>\n\n"

# dear systemd. these services are controlled by supervise
printf "\n >> remove php control from systemd and start php server with supervise >>\n#########################\n"
systemctl stop php8.2-fpm && systemctl mask php8.2-fpm && systemctl daemon-reload && rm /etc/init.d/php8.2-fpm
mkdir /etc/servers/php8-fpm
cat svc-run-php > /etc/servers/php8-fpm/run
chmod +x /etc/servers/php8-fpm/run
ln -s /etc/servers/php8-fpm /etc/service/php8-fpm
printf "\n >> done >>\n\n"

printf "\n >> install mariadb >>\n#########################\n"
echo mariadb-server-10.6 mysql-server/root_password password 'raspberry' | debconf-set-selections
echo mariadb-server-10.6 mysql-server/root_password_again password 'raspberry' | debconf-set-selections
apt-get install -y mariadb-server
printf "\n >> done >>\n\n"

# dear systemd. these services are controlled by supervise
printf "\n >> remove mariadb control from systemd and start mariadb server with supervise >>\n#########################\n";
systemctl stop mariadb && systemctl mask mariadb && systemctl daemon-reload
printf "\n >> done >>\n\n"

printf "\n >> create directories for mariadb and start the mariadb service  >>\n#########################\n"
mkdir /etc/servers/mariadb
cat svc-run-mariadb > /etc/servers/mariadb/run;
chmod +x /etc/servers/mariadb/run
ln -s /etc/servers/mariadb /etc/service/mariadb
printf "\n >> done >>\n\n"

printf "\n >> create directories for nginx and start the nginx service  >>\n#########################\n"
sed -i -e 's/www-data/web/g' /etc/nginx/nginx.conf
echo "daemon off;" >> /etc/nginx/nginx.conf
cat config-nginx > /etc/nginx/sites-enabled/default
printf "\n >> done >>\n\n"

# dear systemd. these services are controlled by supervise
printf "\n >> remove nginx control from systemd and start nginx server with supervise >>\n#########################\n";
systemctl stop nginx && systemctl mask nginx && systemctl daemon-reload && rm /etc/init.d/nginx

mkdir /etc/servers/nginx
cat svc-run-nginx > /etc/servers/nginx/run
chmod +x /etc/servers/nginx/run
ln -s /etc/servers/nginx /etc/service/nginx
printf "\n >> done >>\n\n"

printf "\n >> configure and start the samba service  >>\n#########################\n"
(echo raspberry; echo raspberry) | smbpasswd -a -s web
cat config-samba >> /etc/samba/smb.conf
printf "\n >> done >>\n\n"

printf "\n >> initialize measureit database  >>\n#########################\n"
cat createdb.sql | mysql -uroot -praspberry
printf "\n >> done >>\n\n"

printf "\n >> move measureit files to /web  >>\n#########################\n"
mv ../../../measureit /web
chown -R web:web /web
printf "\n >> done >>\n\n"

printf "\n >> configure and start measureit  >>\n#########################\n"
mkdir /etc/servers/measureit
# change back to the pi user home dir where we clone measureit to
cat svc-run-measureit > /etc/servers/measureit/run
chmod +x /etc/servers/measureit/run
ln -s /etc/servers/measureit /etc/service/measureit
printf "\n >> done >>\n\n"

printf "\n >> delete temp. user  >>\n#########################\n"
userdel te
printf "\n >> done >>\n\n"

printf "\n >> clean up the system and shutdown  >>\n#########################\n"
apt-get clean
rm -r /web/measureit/measureit_system_files/install/
rm -r /web/measureit/public/
echo '' > /home/pi/.bash_history
echo '' > /root/.bash_history
shutdown -h now
